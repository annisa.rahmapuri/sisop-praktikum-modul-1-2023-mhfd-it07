#!/bin/bash

# Mengambil waktu sekarang untuk menentukan jumlah gambar yang akan didownload
X=$(date +%H)

# Membuat folder untuk menyimpan gambar
folder_num=$(ls -1d kumpulan_* 2>/dev/null | wc -l)
folder_name="kumpulan_$((folder_num+1))"
mkdir "$folder_name"

# Mendownload gambar sebanyak X kali
if [ "$X" -eq "0" ]; then
    filename="perjalanan_1.jpg"
    wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
else
for i in $(seq 1 "$X"); do
    filename="perjalanan_$i.jpg"
    wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done
fi

# Compress the directory once a day
if [ "$X" = "00:00" ]; then
  zip -r "devil_$(ls -1 | grep -c "^kumpulan_[0-9]*$").zip" "$folder_name"
fi

    previous_date="$current_date"

    # Tunggu 10 jam sebelum download lagi
    sleep 36000
done

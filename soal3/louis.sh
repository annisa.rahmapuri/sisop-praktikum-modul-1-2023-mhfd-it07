#!/bin/bash

if [ ! -d log.txt ]; then
    touch log.txt
fi

if [ ! -d ./users ]; then
    mkdir ./users
fi

if [ ! -d ./users/user.txt ]; then
    touch ./users/user.txt
fi

timestamp="$(date +'%y/%m/%d %H:%M:%S')"

echo "Enter username:"
read username

if grep -q "^${username}:" ./users/user; then
    echo "User already exists"
    echo "$timestamp REGISTER: ERROR User ${username} already exists" >> log.txt
    exit 1
fi

echo "Enter password:"
read -s password

# Password validation
if [ ${#password} -lt 8 ] ; then
    echo "Password harus minimal 8 karakter"
    exit 1
fi

if ! [[ "$password" =~ [a-z] ]]; then
    echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    exit 1
fi

if ! [[ "$password" =~ [A-Z] ]]; then
    echo "Password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    exit 1
fi

if ! [[ "$password" =~ [0-9] ]]; then
    echo "Password harus alphanumeric"
    exit 1
fi

if [[ "$password" =~ $username ]]; then
    echo "Password tidak boleh sama dengan username "
    exit 1
fi

if [[ "$password" =~ chicken|ernie ]]; then
    echo "Password tidak boleh menggunakan kata chicken atau ernie"
    exit 1
fi

# Add user to users.txt
echo "${username}:${password}" >> ./users/users.txt
echo "User ${username} registered successfully"
echo "$timestamp REGISTER: INFO User ${username} registered successfully" >> log.txt

#!/bin/bash

echo "Enter username:"
read username

timestamp="$(date +'%y/%m/%d %H:%M:%S')"


if ! grep -q "^${username}:" ./users/users.txt; then
    echo "User does not exist"
    echo "$timestamp LOGIN: ERROR Failed login attempt on user ${username}" >> log.txt
    exit 1
fi

echo "Enter password:"
read -s password

# Get stored pa
# Check if passwords match
if [[ "$password" != "$check_password" ]]; then
    echo "Incorrect password"
    echo "$timestamp LOGIN: ERROR Failed login attempt on user ${username}" >> log.txt
    exit 1
fi

echo "Welcome Back! ${username} :D"
echo "$timestamp LOGIN: INFO User ${username} logged in" >> log.txt

echo '----- 5 Universitas dengan Ranking Tertinggi di Jepang ----- '
awk -F ',' '/Japan/ {print $1, " ", $2}' UniRank.csv | sort -n | head -n 5

echo ''

echo '----- Universitas dengan Faculty Student Score Terendah di Jepang -----' 
awk -F ',' '/Japan/ {print $9, " ", $2}' UniRank.csv | sort -n | head -n 5

echo ''

echo ' ----- 10 Universitas dengan Employment Outcome Tertinggi di Jepang -----'
awk -F ',' '/Japan/ {print $20, " ", $2}' UniRank.csv | sort -n | head -n 10

echo ''

echo ' ----- Universitas PALING KEREN di Dunia ----- ' 
awk -F ',' '/Keren/ {print $2}' UniRank.csv

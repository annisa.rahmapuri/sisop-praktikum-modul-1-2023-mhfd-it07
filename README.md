# Laporan Praktikum Sistem Operasi - Modul 1

## Soal 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  :

1. Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
2. Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score (fsr score) yang paling rendah diantara Universitas di Jepang. (revisi soal 5 univeristas fsr terendah)
3. Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
4. Bocchi ngefans berat dengan universitas **paling keren** di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

### **1.1**

Kita diberikan data mengenai Ranking Universitas di Dunia dan kami menamainya dengan UniRank.csv. Pada soal poin ini, kita diminta untuk menampilkan 5 Universitas dengan ranking tertinggi yang ada di Jepan. Berikut ialah langkah pengerjaannya :

- Membuat file script bash terlebih dahulu sesuai dengan ketentuan, yaitu university_survey.sh
    <a href="https://ibb.co/Hg0LgX5"><img src="https://i.ibb.co/mzmrzSf/image.png" alt="image" border="0"></a>
    
    - Menggunakan command “nano” + “nama file” yang diinginkan untuk membuat serta mengedit file tersebut secara sekaligus
- Buat script bash sesuai perintah
    
    ```bash
    echo '----- 5 Universitas dengan Ranking Tertinggi di Jepang ----- '
    awk -F ',' '/Japan/ {print $1, " ", $2}' UniRank.csv | sort -n | head -n 5
    ```
    
    Penjelasan Script tersebut :
    
    - Awk : digunakan mengambil catatan/record tertentu dalam sebuah file dan melakukan sebuah/beberapa operasi terhadap catatan/record tersebut.
    - F ‘,’ : command tambahan awk yang berguna untuk memisahkan ‘,’ pada setiap field
    - ‘/Japan/ {print $2, “ “, $1}’ : command untuk mencari data dengan kata kunci “Japan” dan menampilkan kolom kedua dan pertamanya saja dengan pemisah spasi.
    - UniRank.csv : Pada script tersebut, command ini merujuk pada file UniRank.csv
    - | : berguna sebagai pemisah perintah serta penghubung antara output yg sedang dijalankan dari command sebelumnya ke command selanjutnya.
    - Sort -n : perintah untuk mengurutkan dalam urutan numerik dari yang terkecil hingga terbesar.
    - Head -n 5 : command untuk menampilkan hasil dengan 5 baris teratas. Disini sesuai dengan perintah soal, yaitu menampilkan 5 Univeristas dengan Ranking Tertinggi di Jepang. Disini kami tidak memakai command sort karena menurut data UniRank tersebut, sudah berdasarkan urutan ranking dari yang tertinggi hingga terendah.

### **1.2**

Poin selanjutnya, diminta untuk menampilkan Universitas dengan Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. Berikut ialah langkah pengerjaannya :

- Membuka file script bash terlebih dahulu
    - Menggunakan command “nano” + “nama file” yang diinginkan (university_survey.sh) untuk mengedit file tersebut.
- Buat script bash sesuai perintah
    
    ```bash
    echo '----- Universitas dengan Faculty Student Score Terendah di Jepang -----' 
    awk -F ',' '/Japan/ {print $9, " ", $2}' UniRank.csv | sort -n | head -n 5
    ```
    
    Penjelasan Script tersebut :
    
    - Awk : digunakan mengambil catatan/record tertentu dalam sebuah file dan melakukan sebuah/beberapa operasi terhadap catatan/record tersebut.
    - F ‘,’ : command tambahan awk yang berguna untuk memisahkan ‘,’ pada setiap field
    - ‘/Japan/ {print $9, “ “, $2}’ : command untuk mencari data dengan kata kunci “Japan” dan menampilkan kolom ke-9 dan ke-2 saja dengan pemisah spasi.
    - UniRank.csv : Pada script tersebut, command ini merujuk pada file UniRank.csv
    - | : berguna sebagai pemisah perintah serta penghubung antara output yg sedang dijalankan dari command sebelumnya ke command selanjutnya.
    - Sort -n : perintah untuk mengurutkan dalam urutan numerik dari yang terkecil hingga terbesar.
    - Head -n 5 : command untuk menampilkan hasil dengan 5 baris teratas. Disini sesuai dengan perintah soal, yaitu menampilkan 5 Univeristas dengan Ranking Tertinggi di Jepang. Disini kami tidak memakai command sort karena menurut data UniRank tersebut, sudah berdasarkan urutan ranking dari yang tertinggi hingga terendah.

### **1.3**

Poin selanjutnya, diminta untuk mencari 10 Universitas di Jepang dengan Employment 3Outcome Rank (GER Rank) tertinggi. Berikut ialah langkah pengerjaannya :

- Membuka file script bash terlebih dahulu
    - Menggunakan command “nano” + “nama file” yang diinginkan (university_survey.sh) untuk mengedit file tersebut.
- Buat script bash sesuai dengan perintah
    
    ```bash
    echo ' ----- 10 Universitas dengan Employment Outcome Tertinggi di Jepang -----'
    awk -F ',' '/Japan/ {print $20, " ", $2}' UniRank.csv | sort -n | head -n 10
    ```
    
    Penjelasan script tersebut :
    
    - Awk : digunakan mengambil catatan/record tertentu dalam sebuah file dan melakukan sebuah/beberapa operasi terhadap catatan/record tersebut.
    - F ‘,’ : command tambahan awk yang berguna untuk memisahkan ‘,’ pada setiap field
    - ‘/Japan/ {print $20, “ “, $2}’ : command untuk mencari data dengan kata kunci “Japan” dan menampilkan kolom ke-20 dan k-2 dengan pemisah spasi. Ini berguna untuk mempermudah pembacaan hasil output data
    - UniRank.csv : pada script tersebut, command ini merujuk pada file UniRank.csv
    - | : berguna sebagai pemisah perintah serta penghubung antara output yg sedang dijalankan dari command sebelumnya ke command selanjutnya.
    - Sort -n : perintah untuk mengurutkan dalam urutan numerik dari yang terkecil hingga terbesar.
    - Head -n 10 : command untuk menampilkan hasil dengan 5 baris teratas. Disini sesuai dengan perintah soal, yaitu menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank (GER Rank) tertinggi.

### **1.4**

Poin selanjutnya, diminta untuk menampilkan Universitas **paling keren** di dunia. Berikut ialah langkah pengerjaannya :

- Membuka file script bash terlebih dahulu
    - Menggunakan command “nano” + “nama file” yang diinginkan (university_survey.sh) untuk mengedit file tersebut.
- Buat script bash sesuai dengan perintah
    
    ```bash
    echo ' ----- Universitas PALING KEREN di Dunia ----- ' 
    awk -F ',' '/Keren/ {print $2}' UniRank.csv
    ```
    
    Penjelasan script tersebut :
    
    - Awk : digunakan mengambil catatan/record tertentu dalam sebuah file dan melakukan sebuah/beberapa operasi terhadap catatan/record tersebut.
    - F ‘,’ : command tambahan awk yang berguna untuk memisahkan ‘,’ pada setiap field
    - ‘/Keren/ {print $2}’ : command untuk mencari data dengan kata kunci “Keren” dan menampilkan kolom kedua. Ini berguna untuk mempermudah pembacaan hasil output data

### Output
<a href="https://imgbb.com/"><img src="https://i.ibb.co/wc1X2Sc/output-no1.jpg" alt="output-no1" border="0"></a>


## Soal 2

Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.
- Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
    - File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
    - File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)
- Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.


### **2.1**


Kita diminta membuat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Langkah awal yaitu :


- Membuka file script bash terlebih dahulu
    - Menggunakan command “nano” + “nama file” yang diinginkan (kobeni.liburan.sh) untuk mengedit file tersebut.


- Buat script bash sesuai dengan perintah
   
    ```bash
    X=$(date +%H)
    ```
    - Pada bagian ini adalah inisialisasi suatu variabel timestamp yang berisi informasi tanggal dan waktu.


    ```bash
    folder_num=$(ls -1d kumpulan_* 2>/dev/null | wc -l)
    folder_name="kumpulan_$((folder_num+1))"
    mkdir "$folder_name"
    ```
    - Perintah ls -1d kumpulan_* akan menampilkan semua folder yang dimulai dengan "kumpulan_" dan diakhiri dengan apapun di dalam direktori saat ini. Kemudian, perintah 2>/dev/null juga digunakan untuk mengalihkan pesan error pada output ke /dev/null.


    - Hasil dari perintah wc -l akan disimpan ke dalam variabel folder_num. Variabel folder_num akan berisi jumlah folder yang ditemukan di dalam direktori saat ini yang dimulai dengan "kumpulan_".


    - Selanjutnya, pada perintah folder_name="kumpulan_$((folder_num+1))", variabel folder_name akan di-set dengan string "kumpulan_" diikuti dengan nilai dari folder_num yang ditambah 1. Hal ini bertujuan untuk membuat nama folder baru dengan format "kumpulan_n" dimana "n" adalah nomor urut folder yang baru.


    - Terakhir, perintah mkdir "$folder_name" akan membuat folder baru dengan nama yang disimpan pada variabel folder_name di dalam direktori saat ini menggunakan perintah mkdir.


- Untuk kondisi pertama, yaitu apabila pukul 00:00 cukup download 1 gambar saja maka dapat menggunakkan perintah script bash berikut:


    ```bash
    if [ "$X" -eq "0" ]; then
    filename="perjalanan_1.jpg"
    wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
    ```
    - Pada perintah ini, if [ "$X" -eq "0" ]; then, kita melakukan pengecekan apakah nilai dari variabel waktu X sama dengan 0 menggunakan perintah [ yang merupakan perintah untuk melakukan conditional expression di dalam bash. Jika nilai X sama dengan 0, maka akan dilakukan perintah pada blok kode di bawahnya, yaitu mengunduh gambar sebanyak 1 kali.


    - Kemudian menggunakan perintah wget untuk mengunduh gambar dari URL "[https://source.unsplash.com/1600x900/?indonesia](https://source.unsplash.com/1600x900/?indonesia)" dengan menggunakan opsi -O untuk menyimpan gambar ke dalam direktori yang sudah dibuat sebelumnya. File gambar yang diunduh akan diberi nama "perjalanan_1.jpg" dan disimpan di dalam folder yang bernama $folder_name.


- Untuk kondisi kedua, yaitu mengunduh gambar sebanyak X kali dengan X sebagai jam sekarang maka dapat menggunakkan perintah script bash berikut:


    ```bash
    else
    for i in $(seq 1 "$X"); do
    filename="perjalanan_$i.jpg"
    wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
    done
    fi
    ```
    - Pada perintah for i in $(seq 1 "$X"); do, for digunakan untuk melakukan perulangan sebanyak X kali. Perintah seq digunakan untuk menghasilkan urutan angka dari 1 hingga X. Setiap kali melakukan perulangan, variabel i akan di-set dengan nilai dari urutan angka tersebut.


    - Kemudian, pada setiap iterasi perulangan, variabel filename akan di-set dengan string "perjalanan_i.jpg", dimana i adalah nilai dari variabel i. Ini akan menghasilkan nama file yang berbeda pada setiap iterasi.


    - Selanjutnya, perintah wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia" digunakan untuk mengunduh gambar dari URL "https://source.unsplash.com/1600x900/?indonesia" dengan menggunakan opsi -O untuk menyimpan gambar ke dalam direktori yang sudah dibuat sebelumnya. File gambar yang diunduh akan diberi nama yang disimpan pada variabel filename dan disimpan di dalam folder yang bernama $folder_name.


- Untuk kondisi ketiga, Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan maka dapat menggunakkan perintah script bash berikut:


    ```bash
    sleep 36000
    ```
    - Perintah sleep 36000 digunakan untuk memberhentikan eksekusi program selama 36000 detik atau 10 jam.


### **2.2**


Kita diminta membuat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Langkah awal yaitu :


- Buat script bash sesuai dengan perintah
   
    ```bash
    if [ "$X" = "00:00" ]; then
    zip -r "devil_$(ls -1 | grep -c "^kumpulan_[0-9]*$").zip" "$folder_name"
    fi
    ```
    - Jika variabel X memiliki nilai 00:00, maka program akan melanjutkan ke blok kode then. Di dalam blok kode ini, perintah zip digunakan untuk mengompresi isi direktori dengan nama yang sama dengan nilai variabel $folder_name ke dalam sebuah file zip baru dengan format nama "devil_N.zip", dimana N adalah jumlah folder yang sudah ada pada direktori tempat program dijalankan.


    - Perintah ls -1 | grep -c "^kumpulan_[0-9]*$" digunakan untuk mencari jumlah folder yang sudah ada pada direktori tempat program dijalankan dan sudah diberi nama "kumpulan_N", dimana N adalah bilangan bulat.

### Output

<a href="https://ibb.co/wQ99jZs"><img src="https://i.ibb.co/5jNNwXF/Screenshot-2023-03-10-195624.png" alt="Screenshot-2023-03-10-195624" border="0"></a>

## Soal 3

Peter Griffin hendak membuat suatu sistem register pada script **louis.sh** dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script **retep.sh**

- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
    - Minimal 8 karakter
    - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    - Alphanumeric
    - Tidak boleh sama dengan username
    - Tidak boleh menggunakan kata chicken atau ernie
- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
    - Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
    - Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
    - Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
    - Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

Pada soal ini, kami diminta untuk membuat suatu sistem register pada script **louis.sh** dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. dan sistem log in pada script **retep.sh**

### **3.1**

Untuk membuat sistem register yang aman, terdapat beberapa ketentuan untuk password yang harus diinput. Berikut ialah langkah pengerjaannya :

- Membuat file script bash terlebih dahulu sesuai dengan ketentuan, yaitu louis.sh.
    - Menggunakan command “nano” + “nama file” yang diinginkan (louis.sh) untuk membuat serta mengedit file tersebut secara sekaligus
- Buat script bash sesuai perintah password yang diinginkan
    
    ```bash
    #!/bin/bash
    
    if [ ! -d log.txt ]; then
    	touch log.txt
    fi
    
    if [ ! -d ./users ]; then
    	mkdir ./users
    fi
    
    if [ ! -d ./users/user.txt ]; then
    	touch ./users/user.txt
    fi
    ```
    
    - Script tersebut berfungsi untuk mengecek apakah ada file atau directory yang diperlukan oleh program sudah ada atau belum. Jika file atau directory belum ada, maka program akan membuatnya.
    - Pada bagian pertama, script mengecek apakah file log.txt sudah ada, jika belum maka file tersebut akan dibuat menggunakan perintah "touch". Ini juga berlaku untuk poin ketiga yaitu pada file user.txt. Selanjutnya pada bagian kedua, script mengecek apakah directory users sudah ada, jika belum maka directory tersebut akan dibuat menggunakan perintah "mkdir".
    
    ```bash
    timestamp="$(date +'%y/%m/%d %H:%M:%S')"
    ```
    
    - Pada bagian ini adalah inisialisasi suatu variabel timestamp yang berisi informasi tanggal dan waktu.
    
    ```bash
    echo "Enter username:"
    read username
    ```
    
    - Pada script ini berfungsi untuk menginput username. Command echo digunakan untuk menampilkan string "Enter username:" pada layar terminal. Kemudian, command read digunakan untuk mengambil input dari pengguna dan menyimpannya ke dalam variabel username. Ketika pengguna mengetikkan input dan menekan tombol Enter, nilai dari input tersebut akan disimpan dalam variabel username. Variabel username selanjutnya akan digunakan dalam tahapan validasi password dan saat menambahkan pengguna ke dalam file users.txt
    
    ```bash
    if grep -q "^${username}:" ./users/user.txt ; then
    	echo "User already exists"
    	echo "$timestamp REGISTER: ERROR User ${username} already exists" >> log.txt
    	exit 1
    fi
    ```
    
    - Pada script ini berfungsi untuk memeriksa apakah username yang diinput sudah terdapat pada file ./users/user atau belum dengan menggunakan command grep. grep akan mencari username pada file user.txt. Dan jika ada akan mengeluarkan atau echo “user already exists” pada terminal dan akan memasukan "$timestamp REGISTER: ERROR User ${username} already exists" pada file log.txt.
    
    ```bash
    echo "Enter password:"
    read -s password
    ```
    
    - Sama seperti bagian username di atas, bagian ini berfungsi untuk menginput data password masing masing username. commad `-s` berguna untuk menjaga karakter yang diketik tidak tampil pada layar (secret mode)
    
    ```bash
    if [ ${#password} -lt 8 ] ; then
    	echo "Password harus minimal 8 karakter"
    	exit 1
    fi
    ```
    
    - Pada bagian di atas berfungsi sebagai password validation poin pertama. Script ini akan memeriksa panjang karakter password yang diinput. Tanda -lt berarti sebagai pembanding kurang dari. Jika password tersebut kurang dari 8 karakter, maka akan mengeluarkan kalimat “Password harus minimal 8 karakter”.
    
    ```bash
    if ! [[ "$password" =~ [a-z] ]]; then
    	echo "Password harus memiliki minimal 1 huruf kecil "
    	exit 1
    fi
    
    if ! [[ "$password" =~ [A-Z] ]]; then
    	echo "Password harus memiliki minimal 1 huruf kapital"
    	exit 1
    fi
    ```
    
    - Pada pengondisian ini, program akan memeriksa apakah variable password setidaknya memiliki satu huruf kecil (pada if atas) dan kapital (pada if bawah). Operator “=~” digunakan untuk mencocokan antara variable password dengan [a-z] dan [A-Z] sesuai atau tidak. Jika tidak, akan masing masing mengeluarkan pesan "Password harus memiliki minimal 1 huruf kapital" atau "Password harus memiliki minimal 1 huruf kecil " sesuai dengan errornya.
    
    ```bash
    if ! [[ "$password" =~ [0-9] ]]; then
    	echo "Password harus berupa Alphanumeric"
    	exit 1
    fi
    ```
    
    - Pada bagian ini, program akan memerikan lagi apakah username memiliki setidaknya satu digit angka atau tidak. Ini berguna untuk memenuhi ketentuan password harus alphanumeric. Kami membuat pengecekan angka karena di pengecekan sebelumnya sudah terfilter harus memiliki minimal huruf Kapital dan kecil sehingga jika sudah berhasul masuk ke pengecekan keduanya sudah dapat dikatakan alphanumeric.
    
    ```bash
    if [[ "$password" =~ $username ]]; then
    	echo "Password tidak boleh sama dengan username "
    	exit 1
    fi
    ```
    
    - Script ini berguna untuk memeriksa apakah variabel input password dan username adalah yang sama atau tidak. Hal ini akan menjawab ketentuan password yaitu "Password tidak boleh sama dengan username "
    
    ```bash
    if [[ "$password" =~ chicken|ernie ]]; then
    	echo "Password tidak boleh menggunakan kata chicken atau ernie'"
    	exit 1
    fi
    ```
    
    - Bagian ini berguna sebagai loop pemeriksa ketentuan "Password tidak boleh menggunakan kata chicken atau ernie'". Jika terdapat kesamaan, maka proggram akan mengeluarkan echo yang ada pada script tersebut akan keluar dengan kode kesalahan 1 atau menunjukan bahwa terjadi kegagalan pada proggram
    
    ```bash
    echo "${username}:${password}" >> /users/users.txt
    ```
    
    - Proses pengecekan password selesai, selajutnya masuk ke proses menambahkan data username dan password ke dalam file /users/users.txt hal tersebut ditunjukan dengan operator “>>” atau yang disebut juga operator redirection.
    
    ```bash
    echo "User ${username} registered successfully"
    ```
    
    - Setelah data username dan password sudah tersimpan, script ini akan menampilkan kalimat "User ${username} registered successfully" pada terminal sebagai respon dari proggram yang dibuat.
    
    ```bash
    echo "$timestamp REGISTER: INFO User ${username} registered successfully" >> log.txt
    ```
    
    - Terakhir, pada script ini akan membuat massage "$timestamp REGISTER: INFO User ${username} registered successfully" pada file log.txt

### 3.2

Setelah membuat sistem register, kami diminta juga untuk membuat sistem log in dengan setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user. Berikut langkah pengerjaannya: 

- Membuat file script bash terlebih dahulu sesuai dengan ketentuan, yaitu [retep.sh](http://retep.sh)
    - Menggunakan command “nano” + “nama file” yang diinginkan (retep.sh) untuk membuat serta mengedit file tersebut secara sekaligus
- Buat script bash sesuai perintah password yang diinginkan
    
    ```bash
    #!/bin/bash
    
    echo "Enter username:"
    read username
    ```
    
    - Pada script ini berfungsi untuk menginput username. Command echo digunakan untuk menampilkan string "Enter username:" pada layar terminal. Kemudian, command read digunakan untuk mengambil input dari pengguna dan menyimpannya ke dalam variabel username. Ketika pengguna mengetikkan input dan menekan tombol Enter, nilai dari input tersebut akan tersimpan dalam variabel username.
    
    ```bash
    timestamp="$(date +'%y/%m/%d %H:%M:%S')"
    ```
    
    - Pada bagian ini adalah inisialisasi suatu variabel timestamp yang berisi informasi tanggal dan waktu.
    
    ```bash
    if ! grep -q "^${username}:" ./users/users.txt; then
    	echo "User does not exist"
    	echo "$timestamp LOGIN: ERROR Failed login attempt on user ${username}" >> log.txt
    	exit 1
    fi
    ```
    
    - Pada script ini digunakan untuk memeriksa apakah username yang dimasukkan sudah terdaftar pada "users.txt". Jika tidak ada, maka akan dicetak pesan "User does not exist" dan  mencatat log berisi “(timestamp) LOGIN: ERROR Failed login attempt on user (username)" ke file "log.txt".
    
    ```bash
    echo "Enter password:"
    read -s password
    ```
    
    - Pada script ini mencetak pesan "Enter password:" ke layar dan membaca input yang diberikan pengguna dari keyboard. Input password tersebut akan ditampung dalam variabel password. Perintah "-s" pada perintah read membuat password yang diinput** tidak ditampilkan di layar yang berguna untuk menjaga keamanan password.
    
    ```bash
    if [[ "$password" != "$check_password" ]]; then
    	echo "Incorrect password"
    	echo "$timestamp LOGIN: ERROR Failed login attempt on user ${username}" >> log.txt
    	exit 1
    fi
    ```
    
    - Pada script bagian ini membandingkan nilai variabel "password" dengan nilai variabel "check_password". Jika keduanya tidak sama, maka script akan menampilkan pesan "Incorrect password", mencatat log bahwa ada upaya login yang gagal pada username yang diinput.
    - Perintah "[[]]" (double bracket) digunakan untuk melakukan perbandingan string atau kondisi lainnya dalam Bash. Operator "!=" menandakan tidak sama dengan, sehingga kondisi tersebut hanya akan terpenuhi jika password dan check_password tidak sama.
    
    ```bash
    echo "Welcome Back ${username} :D"
    echo "$timestamp LOGIN: INFO User ${username} logged in" >> log.tx
    ```
    
    - Jika kondisi pada baris pertama tidak terpenuhi (artinya password benar), maka pesan "Welcome" beserta username yang berhasil login akan ditampilkan ke layar dan mencatat log ke file "log.txt" bahwa pengguna dengan username tersebut telah berhasil melakukan login.

### Output

***Log.txt*** 
<a href="https://ibb.co/kcwhCGb"><img src="https://i.ibb.co/RHsCwgr/outoput2.png" alt="outoput2" border="0"></a>

***User.txt*** 
<a href="https://ibb.co/7Kv2Z6M"><img src="https://i.ibb.co/2dqgCfV/Screenshot-2023-03-10-200323.png" alt="Screenshot-2023-03-10-200323" border="0"></a>

***Register Error***
<a href="https://imgbb.com/"><img src="https://i.ibb.co/8d8nHYZ/Screenshot-2023-03-10-202524.png" alt="Screenshot-2023-03-10-202524" border="0"></a>

***Register Succes***
<a href="https://imgbb.com/"><img src="https://i.ibb.co/M1cppws/Screenshot-2023-03-10-202840.png" alt="Screenshot-2023-03-10-202840" border="0"></a>

***Login Error***
<a href="https://imgbb.com/"><img src="https://i.ibb.co/bggbNRh/Screenshot-2023-03-10-203131.png" alt="Screenshot-2023-03-10-203131" border="0"></a>

***Login Succes***
<a href="https://imgbb.com/"><img src="https://i.ibb.co/SRcwz1M/Screenshot-2023-03-10-203027.png" alt="Screenshot-2023-03-10-203027" border="0"></a>


## Soal 4


Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan :


- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).






- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    -Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    -Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    -Setelah huruf z akan kembali ke huruf a


- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀


Jadi pada soal diminta untuk membuat backup syslog dengan format jam:menit tanggal:bulan:tahun.txt. Backup itu minta dienkripsi dengan cipherkeynya adalah jam pada saat program dijalankan dan juga program dijalankan setiap 2 jam. Lalu diminta juga untuk membuat deskripsi dari enkripsi tersebut.


### 4.1 log_encrypt
Membuat File log_encrypt untuk mengenkripsi syslog:
- Membuat file script bash terlebih dahulu sesuai dengan ketentuan, yaitu log_encrypt.sh.
    - Menggunakan command “nano” + “nama file” yang diinginkan (log_encrypt.sh) untuk membuat serta mengedit file tersebut secara sekaligus
- Buat script bash untuk mengenkripsi syslog:


```bash
#!/bin/bash


# Ambil waktu saat ini dan sesuaikan dengan format waktu untuk nama file
TIME=$(date +"%H:%M_%d:%m:%Y")
FILENAME="syslog_$TIME.txt"

```
Variabel TIME diisi dengan waktu saat ini dalam format "jam:menit_tanggal:bulan:tahun", contohnya "09:30_09:03:2023". Variabel FILENAME diisi dengan nama file yang akan dibuat dari waktu tersebut dengan menambahkan prefix "syslog_" di depannyadana diakhir dengan format “.txt” diakhirnya


```bash
# Enkripsi isi file dengan cipher sesuai dengan jam saat ini
CIPHER=$(printf "%s" "$(date +"%H")")
ENCRYPTED=$(tr '[a-z]' "$(echo {a..z} | tr -d ' ' | cut -b$((CIPHER+1))-$(echo {a..z} | wc -c))$(echo {a..z} | tr -d ' ' | cut -b1-$CIPHER)" <<< "$(cat /var/log/syslog)"
```
Variabel CIPHER diisi dengan jam saat ini dalam format dua digit (00-23) menggunakan perintah date. Variabel ENCRYPTED diisi dengan isi file /var/log/syslog yang dienkripsi dengan menggunakan cipher sesuai dengan jam saat ini. Cipher digunakan untuk menggeser huruf-huruf di dalam alfabet, sehingga setiap huruf diganti dengan huruf lain yang berada pada posisi yang digeser di alfabet.


```bash
# Simpan isi file yang telah dienkripsi ke file backup
BACKUP_DIR="/backup"
if [ ! -d "$BACKUP_DIR" ]; then
  mkdir "$BACKUP_DIR"
fi


echo "$ENCRYPTED" > "$BACKUP_DIR/$FILENAME"
```
Blok kode ini memeriksa apakah direktori bernama /backup ada, dan membuatnya jika tidak ada. Kemudian, kode ini menulis konten terenkripsi dari /var/log/syslog ke file baru dengan nama file syslog_$TIME.txt di dalam direktori /backup.


```bash
echo "Backup file syslog berhasil dilakukan pada $TIME."
echo "0 */2 * * * $USER /home/testing/log_encrypt.sh" | crontab -
```
Dua baris ini hanya mencetak pesan yang menunjukkan bahwa proses backup telah berhasil, dan kemudian mengatur cron job baru untuk menjalankan script log_encrypt.sh setiap dua jam.


Jalankan program dengan mengetik pada terminal 
```
Bash log_encrypt.sh
```
## 4.2 log_decrypt


Membuat File log_decrypt untuk mendekripsi file enkripsi:
- Membuat file script bash terlebih dahulu sesuai dengan ketentuan, yaitu log_encrypt.sh.
    - Menggunakan command “nano” + “nama file” yang diinginkan (log_encrypt.sh) untuk membuat serta mengedit file tersebut secara sekaligus
- Buat script bash untuk mendekripsi file enkripsi:
```bash
#!/bin/bash


# Ambil waktu dari nama file backup
TIME=$(echo $1 | awk -F '_' '{print $2}')
```
Baris ini mengambil waktu dari nama file backup yang diberikan sebagai argumen pada saat menjalankan script. Waktu diambil dengan memisahkan nama file menjadi beberapa bagian, yang dipisahkan oleh karakter _, dan kemudian mengambil elemen kedua yang merupakan waktu.
```bash
# Dekripsi isi file dengan cipher sesuai dengan jam saat backup
CIPHER=$(printf "%s" "$TIME" | cut -b1-2)
DECRYPTED=$(tr "$(echo {a..z} | tr -d ' ' | cut -b$((CIPHER+1))-$(echo {a..z} | wc -c))$(echo {a..z} | tr -d ' ' | cut -b1-$CIPHER)" '[a-z]' <<< "$(cat $1)")
```
Baris ini mendekripsi isi file backup yang diberikan sebagai argumen. Pertama, baris ini mengambil nilai cipher yang digunakan pada saat backup, yaitu dua karakter pertama dari waktu backup. Selanjutnya, baris ini menghitung karakter substitusi yang akan digunakan dalam Caesar cipher, menggunakan karakter-karakter alfabet dari a sampai z. Karakter-karakter ini digunakan sebagai argumen pertama dari command tr, sedangkan argumen kedua adalah range karakter-karakter alfabet dalam huruf kecil. Setelah karakter substitusi ditentukan, baris ini menggunakan command tr untuk melakukan dekripsi isi file backup yang dienkripsi dengan Caesar cipher.


```bash
# Simpan isi file yang telah didekripsi ke file baru
echo "$DECRYPTED" > "${1%.txt}_decrypted.txt"
echo "File berhasil didekripsi."
```


Baris ini menyimpan isi file yang telah didekripsi ke dalam file baru dengan nama yang sama dengan file backup, namun dengan tambahan suffix _decrypted. Lalu menyatakan bahwa file berhasil didekripsi.


Jalankan program dengan mengetik pada terminal 
```
Bash log_decrypted.sh /namafile.txt
```

### Output

***log_encrypt***
<a href="https://ibb.co/0pbbsmv"><img src="https://i.ibb.co/TD33WYd/Screenshot-2023-03-10-211710.png" alt="Screenshot-2023-03-10-211710" border="0"></a>


***log_decrypt***
<a href="https://ibb.co/2YVvY0D"><img src="https://i.ibb.co/yBv6Bmw/Screenshot-2023-03-10-211522.png" alt="Screenshot-2023-03-10-211522" border="0"></a>

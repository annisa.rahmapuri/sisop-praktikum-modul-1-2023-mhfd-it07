#!/bin/bash

# Ambil waktu dari nama file backup
TIME=$(echo $1 | awk -F '_' '{print $2}')

# Dekripsi isi file dengan cipher sesuai dengan jam saat backup
CIPHER=$(printf "%s" "$TIME" | cut -b1-2)
DECRYPTED=$(tr "$(echo {a..z} | tr -d ' ' | cut -b$((CIPHER+1))-$(echo {a..z} | wc -c))$(echo {a..z} | tr -d ' ' | cut -b1-$CIPHER)" '[a-z]' <<< "$(cat $1)")

# Simpan isi file yang telah didekripsi ke file baru
echo "$DECRYPTED" > "${1%.txt}_decrypted.txt"

echo "File berhasil didekripsi."

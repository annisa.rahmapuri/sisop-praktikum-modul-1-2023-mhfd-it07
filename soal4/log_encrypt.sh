#!/bin/bash

# Ambil waktu saat ini dan sesuaikan dengan format waktu untuk nama file
TIME=$(date +"%H:%M_%d:%m:%Y")
FILENAME="syslog_$TIME.txt"

# Enkripsi isi file dengan cipher sesuai dengan jam saat ini
CIPHER=$(printf "%s" "$(date +"%H")")
ENCRYPTED=$(tr '[a-z]' "$(echo {a..z} | tr -d ' ' | cut -b$((CIPHER+1))-$(echo {a..z} | wc -c))$(echo {a..z} | tr -d ' ' | cut -b1-$CIPHER)" <<< "$(cat /var/log/syslog)")

# Simpan isi file yang telah dienkripsi ke file backup
BACKUP_DIR="/backup"
if [ ! -d "$BACKUP_DIR" ]; then
  mkdir "$BACKUP_DIR"
fi

echo "$ENCRYPTED" > "$BACKUP_DIR/$FILENAME"

echo "Backup file syslog berhasil dilakukan pada $TIME."
echo "0 */2 * * * $USER /home/testing/log_encrypt.sh" | crontab -
